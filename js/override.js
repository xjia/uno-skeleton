$(function(){
  $('.tooltipped.upwards').tooltip({placement: 'top'});
  $('.tooltipped.downwards').tooltip({placement: 'bottom'});
  $('.tooltipped.leftwards').tooltip({placement: 'left'});
  $('.tooltipped.rightwards').tooltip({placement: 'right'});
  
  $('.sticky').sticky();
  
  var oldOnScroll = window.onscroll;
  $('.modal').on('show', function(){
    var x = window.scrollX,
        y = window.scrollY;
    window.onscroll = function(){
      window.scrollTo(x, y);
    };
  }).on('hide', function(){
    window.onscroll = oldOnScroll;  // restore
  });
});
