(function($){
  $.fn.extend({
    sticky: function(){
      return this.each(function(){
        var elem = $(this);
        
        var oldWidth = elem.css('width');
        var oldHeight = elem.css('height');
        var offset = elem.offset();
        var oldMarginTop = elem.css('marginTop');
        var oldTop = elem.css('top');
        var oldPosition = elem.css('position');
        var oldZIndex = elem.css('zIndex');
        var fixZIndex = 1000;  // should be smaller than Bootstrap overlay z-index
        
        var threshold = parseInt(elem.data('threshold'));
        var topMargin = parseInt(elem.data('top-margin'));
        if (threshold === null || isNaN(threshold)) threshold = 0;
        if (topMargin === null || isNaN(topMargin)) topMargin = 0;
        
        var scrollable = true;
        
        var scroller = function(){
          var scrollTop = $(window).scrollTop();
          if (scrollable && scrollTop + topMargin + threshold >= offset.top) {
            elem.css({
              width: oldWidth,
              height: oldHeight,
              marginTop: topMargin,
              top: 0,
              position: 'fixed',
              zIndex: fixZIndex
            });
          } else {
            elem.css({
              width: oldWidth,
              height: oldHeight,
              marginTop: oldMarginTop,
              top: oldTop,
              position: oldPosition,
              zIndex: oldZIndex
            });
          }
        };
        
        var resizer = function(){
          elem.css({
            width: 'auto',
            height: 'auto',
            marginTop: oldMarginTop,
            top: oldTop,
            position: oldPosition,
            zIndex: oldZIndex
          });
          
          oldWidth = elem.css('width');
          oldHeight = elem.css('height');
          offset = elem.offset();
          oldMarginTop = elem.css('marginTop');
          oldTop = elem.css('top');
          oldPosition = elem.css('position');
          oldZIndex = elem.css('zIndex');
          
          if (topMargin + parseInt(oldHeight) <= $(window).height()) {
            scrollable = true;
          } else {
            scrollable = false;
          }
          
          var scrollTop = $(window).scrollTop();
          if (scrollTop + topMargin + threshold >= offset.top) {
            elem.css({
              width: oldWidth,
              height: oldHeight,
              marginTop: topMargin,
              top: 0,
              position: 'fixed',
              zIndex: fixZIndex
            });
          }
        };
        
        // should be more efficient than using $(window).scroll(scroller) and $(window).resize(resizer):
        if (window.addEventListener) {
          window.addEventListener('scroll', scroller, false);
          window.addEventListener('resize', resizer, false);
        } else if (window.attachEvent) {
          window.attachEvent('onscroll', scroller);
          window.attachEvent('resize', resizer);
        }
        
        $(function(){
          setTimeout(scroller, 0);
        });
      });
    }
  });
})(jQuery);
