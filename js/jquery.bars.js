(function($){
  $.fn.extend({
    bars: function(){
      var ParticipationGraph = (function(){
        var b = function(target){
          this.el = target;
          this.onSuccess = $.proxy(this.onSuccess, this);
          this.canvas = this.el.getContext('2d');
          this.refresh();
        }
        b.prototype.barWidth = 7;
        b.prototype.barMaxHeight = 20;
        b.prototype.getUrl = function(){
          return $(this.el).data('source');
        };
        b.prototype.setData = function(data){
          this.data = data;
          if (data == null || data.all == null || data.owner == null) {
            this.data = null;
          }
          this.scale = this.getScale(this.data);
        };
        b.prototype.getScale = function(data){
          var mx, i;
          if (data == null) return;
          mx = data.all[0];
          for(i = 0; i < data.all.length; i++) {
            if (data.all[i] > mx) {
              mx = data.all[i];
            }
          }
          return mx >= this.barMaxHeight ? (this.barMaxHeight-.1)/mx : 1;
        };
        b.prototype.refresh = function(){
          $.ajax({
            url: this.getUrl(),
            dataType: 'json',
            success: this.onSuccess
          });
        };
        b.prototype.onSuccess = function(data){
          this.setData(data);
          this.draw();
        };
        b.prototype.draw = function(){
          if (this.data == null) return;
          this.drawCommits(this.data.all, '#cacaca');
          this.drawCommits(this.data.owner, '#336699');
        };
        b.prototype.drawCommits = function(data, color){
          var i, width, height, x, y;
          for (i = 0; i < data.length; i++) {
            width = this.barWidth;
            height = data[i] * this.scale;
            x = i * (this.barWidth + 1);
            y = this.barMaxHeight - height;
            this.canvas.fillStyle = color;
            this.canvas.fillRect(x, y, width, height);
          }
        };
        return b;
      })();
      return this.each(function(){
        new ParticipationGraph(this);
      });
    }
  });
})(jQuery);
